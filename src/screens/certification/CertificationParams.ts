import { User } from "../../models/User";

export class CertificationParams {
  private _user: User = new User();

  get user(): User {
    return this._user;
  }

  set user(value: User) {
    this._user = value;
  }
}
